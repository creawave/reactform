import {
    SET_FIRST_PAGE,
    SET_FOUR_PAGE,
    SET_SECOND_PAGE,
    SET_THIRD_PAGE
} from "../actionsTypes";

export const setFirstPage = (title, dsc, status) => ({
    type: SET_FIRST_PAGE,
    payload: { title, dsc, status },
});

export const setSecondPage = (phone, email) => ({
    type: SET_SECOND_PAGE,
    payload: { phone, email },
});

export const setThirdPage = (img) => ({
    type: SET_THIRD_PAGE,
    payload: { img },
});

export const setFourPage = (service) => ({
    type: SET_FOUR_PAGE,
    payload: { service },
});