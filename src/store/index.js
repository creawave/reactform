import React from "react";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { formDataReducer } from "./reducers";

const rootReducers = combineReducers({
    formDataReducer
});

const middleware = [thunk];
const middlewareEnhancer = applyMiddleware(...middleware);

const store = createStore(rootReducers, composeWithDevTools(middlewareEnhancer));



export default store;