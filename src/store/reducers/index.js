import {
    SET_FIRST_PAGE,
    SET_FOUR_PAGE,
    SET_SECOND_PAGE,
    SET_THIRD_PAGE
} from "../actionsTypes";

const initialState = {
    FormData: {},
};

export const formDataReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case SET_FIRST_PAGE:
            return {
                ...state,
                FormData: {...state.FormData, ...payload},
            };
        case SET_SECOND_PAGE:
            return {
                ...state,
                FormData: {...state.FormData, ...payload},
            };
        case SET_THIRD_PAGE:
            return {
                ...state,
                FormData: {...state.FormData, ...payload},
            };
        case SET_FOUR_PAGE:
            return {
                ...state,
                FormData: {...state.FormData, ...payload},
            };
        default:
            return state;
    }
};