import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Switcher from "./components/Switcher/Switcher";

const App = () => {
  return (
      <BrowserRouter>
          <Switcher/>
      </BrowserRouter>
  );
};




export default App;
