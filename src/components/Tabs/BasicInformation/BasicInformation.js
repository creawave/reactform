import React, { useState } from 'react';
import {
    Button,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import { useDispatch } from "react-redux";
import { setFirstPage } from "../../../store/actions";


const BasicInformation = ({ toggle }) => {
    const [title, setTitle] = useState('');
    const [dsc, setDsc] = useState('');
    const [status, setStatus] = useState('');
    const [invalid, setInvalid] = useState(false);
    const dispatch = useDispatch();
    return (
        <Form className='p-2 bg-light border'>
            <FormGroup>
                <Label for="title">Заголовок</Label>
                <Input
                    invalid={invalid}
                    type="text"
                    id="title"
                    value={title}
                    onClick={() => setInvalid(false)}
                    onChange={({target:{ value }}) => setTitle(value)}
                />
                <FormFeedback>Required field!</FormFeedback>
            </FormGroup>
            <FormGroup>
                <Label for="dsc">Описание</Label>
                <Input
                    type="textarea"
                    id="dsc"
                    value={dsc}
                    onChange={({target:{ value }}) => setDsc(value)}
                />
            </FormGroup>
            <FormGroup tag="fieldset">
                <FormGroup check>
                    <Label check>
                        <Input type="radio" value='On' name="radio1" onClick={({target:{ value }}) => setStatus(value)}/>
                        On
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="radio" value='Off' name="radio1" onClick={({target:{ value }}) => setStatus(value)}/>
                        Off
                    </Label>
                </FormGroup>
            </FormGroup>
            <div className='p-2 bg-dark d-flex justify-content-end'>
                <Button
                    color="primary"
                    onClick={() => {
                        if (title !== '') {
                            toggle('2');
                            dispatch(setFirstPage(title, dsc, status));
                        } else {
                            setInvalid(true);
                        }
                    }}
                >
                    Next
                </Button>
            </div>
        </Form>
    )
};

export default BasicInformation;