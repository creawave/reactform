import React, { useState } from 'react';
import {
    Button,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import {useDispatch, useSelector} from "react-redux";
import {setFourPage} from "../../../store/actions";

const CheckBox = ({ toggle }) => {
    const [services, setService] = useState('');
    const [visible, setVisible] = useState(false);
    const dispatch = useDispatch();
    const { title, dsc, status, phone, email, img, service } = useSelector(state => state.formDataReducer.FormData);
    return (
        <div>
            <Form className='p-2 bg-light border'>
                <FormGroup tag="fieldset">
                    <legend>Публикация:</legend>
                    <FormGroup check className='pb-2'>
                        <Label check>
                            <Input
                                type="radio"
                                name="radio1"
                                value='Услуга 1'
                                onClick={({target:{ value }}) => setService(value)}
                            />
                            Услуга 1
                        </Label>
                    </FormGroup>
                    <FormGroup check className='pb-2'>
                        <Label check>
                            <Input
                                type="radio"
                                name="radio1"
                                value='Услуга 2'
                                onClick={({target:{ value }}) => setService(value)}
                            />
                            Услуга 2
                        </Label>
                    </FormGroup>
                    <FormGroup check className='pb-2'>
                        <Label check>
                            <Input
                                type="radio"
                                name="radio1"
                                value='Услуга 3'
                                onClick={({target:{ value }}) => setService(value)}
                            />
                            Услуга 3
                        </Label>
                    </FormGroup>
                    <FormGroup check className='pb-2'>
                        <Label check>
                            <Input
                                type="radio"
                                name="radio1"
                                value='Услуга 4'
                                onClick={({target:{ value }}) => setService(value)}
                            />
                            Услуга 4
                        </Label>
                    </FormGroup>
                    <FormGroup check className='pb-2'>
                        <Label check>
                            <Input
                                type="radio"
                                name="radio1"
                                value='Услуга 5'
                                onClick={({target:{ value }}) => setService(value)}
                            />
                            Услуга 5
                        </Label>
                    </FormGroup>
                </FormGroup>
                <div className='p-2 bg-dark d-flex justify-content-between'>
                    <Button color="danger" onClick={() => { toggle('3') }}>Prev</Button>
                    <Button
                        color="success"
                        onClick={() => {
                            dispatch(setFourPage(services));
                            setVisible(true);
                        }}
                    >
                        Save
                    </Button>
                </div>
            </Form>
            {
                visible &&
                <div className=' d-flex flex-column p-5 border bg-light text-left'>
                    <div className=''>Title: {title}</div>
                    <div>Description: {dsc === '' ? 'Пусто' : dsc}</div>
                    <div>Status: {status === '' ? 'Пусто' : status}</div>
                    <div>Phone: {phone}</div>
                    <div>Email: {email === '' ? 'Пусто' : email}</div>
                    <div>Image: {img === '' ? 'Пусто' : img.map(item => <div>{item}</div>)}</div>
                    <div>Service: {service === '' ? 'Пусто' : service}</div>
                </div>
            }

        </div>
    )
};

export default CheckBox;