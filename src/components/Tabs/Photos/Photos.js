import React, { useState } from 'react';
import {
    Button,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import { useDispatch } from "react-redux";
import { setThirdPage } from "../../../store/actions";

const Photos = ({ toggle }) => {
    const dispatch = useDispatch();
    return (
        <FormGroup className='p-2 bg-light border'>
            <Label for="file">Фотография</Label>
            <Input
                type="file"
                id="file"
                className='pb-5'
                multiple
                onChange={(e) => {
                    if(e.currentTarget.files.length > 5) {
                        alert('Загрузка до 5 фотографий');
                        e.currentTarget.value = null;
                    }
                    dispatch(setThirdPage(Object.entries(e.currentTarget.files).map(item => item[1].name)))
                }}
            />
            <div className='p-2 bg-dark d-flex justify-content-between'>
                <Button color="danger" onClick={() => { toggle('2') }}>Prev</Button>
                <Button
                    color="primary"
                    onClick={() => {
                        toggle('4');
                    }}
                >
                    Next
                </Button>
            </div>
        </FormGroup>
    )
};

export default Photos;