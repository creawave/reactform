import React, { useState } from 'react';
import {
    Button,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import { useDispatch } from "react-redux";
import { setSecondPage } from "../../../store/actions";

const ContactInformation = ({ toggle }) => {
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [invalid, setInvalid] = useState(false);
    const dispatch = useDispatch();
    return (
        <Form className='p-2 bg-light border'>
            <FormGroup>
                <Label for="phone">Номер телефона</Label>
                <Input
                    invalid={invalid}
                    type="text"
                    id="phone"
                    required
                    value={phone}
                    onClick={() => setInvalid(false)}
                    onChange={({target:{ value }}) => setPhone(value)}
                />
                <FormFeedback>Required field!</FormFeedback>
            </FormGroup>
            <FormGroup>
                <Label for="email">Емайл</Label>
                <Input
                    type="text"
                    id="email"
                    value={email}
                    onChange={({target:{ value }}) => setEmail(value)}
                />
            </FormGroup>
            <div className='p-2 bg-dark d-flex justify-content-between'>
                <Button color="danger" onClick={() => { toggle('1') }}>Prev</Button>
                <Button
                    color="primary"
                    onClick={() => {
                        if (phone !== '') {
                            toggle('3');
                            dispatch(setSecondPage(phone, email));
                        } else {
                            setInvalid(true)
                        }
                    }}
                >
                    Next
                </Button>
            </div>
        </Form>
    )
};

export default ContactInformation;