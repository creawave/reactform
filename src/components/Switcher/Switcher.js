import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Container } from 'reactstrap';
import classnames from 'classnames';
import BasicInformation from "../Tabs/BasicInformation/BasicInformation";
import ContactInformation from "../Tabs/ContactInformation/ContactInformation";
import Photos from "../Tabs/Photos/Photos";
import CheckBox from "../Tabs/CheckBoxs/CheckBoxs";


const Switcher = () => {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    };
    return (
        <Container className='p-5'>
            <div>
                <Nav tabs>
                    <NavItem>
                        <NavLink className={classnames({ active: activeTab === '1' })}>
                            Tab 1
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={classnames({ active: activeTab === '2' })}>
                            Tab 2
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={classnames({ active: activeTab === '3' })}>
                            Tab 3
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={classnames({ active: activeTab === '4' })}>
                            Tab 4
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                        <BasicInformation toggle={toggle}/>
                    </TabPane>
                    <TabPane tabId="2">
                        <ContactInformation toggle={toggle}/>
                    </TabPane>
                    <TabPane tabId="3">
                        <Photos toggle={toggle}/>
                    </TabPane>
                    <TabPane tabId="4">
                        <CheckBox toggle={toggle}/>
                    </TabPane>
                </TabContent>
            </div>
        </Container>
    )
};

export default Switcher;